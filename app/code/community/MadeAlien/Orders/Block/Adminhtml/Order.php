<?php


class MadeAlien_Orders_Block_Adminhtml_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_order";
        $this->_blockGroup = "ma_orders";
        $this->_headerText = Mage::helper("ma_orders")->__("MadeAlien Orders");
        parent::__construct();
        $this->_removeButton('add');
    }

}