<?php

class MadeAlien_Orders_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId("orderGrid");
        $this->setDefaultSort("ma_order_log_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("ma_orders/order")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("ma_order_log_id", array(
            "header" => Mage::helper("ma_orders")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "ma_order_log_id",
        ));

        $this->addColumn("order_id", array(
            "header" => Mage::helper("ma_orders")->__("Order ID"),
            "index" => "order_id",
            'type' => 'number',
        ));
        $this->addColumn("order_increment_id", array(
            "header" => Mage::helper("ma_orders")->__("Order Increment ID"),
            "index" => "order_increment_id",
            'type' => 'text',
        ));
        $this->addColumn("order_value", array(
            "header" => Mage::helper("ma_orders")->__("Order Value"),
            "index" => "order_value",
            'type' => 'number',
        ));
        $this->addColumn("created", array(
            "header" => Mage::helper("ma_orders")->__("Created"),
            "index" => "created",
            'type' => 'datetime',
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return '#';
    }


}