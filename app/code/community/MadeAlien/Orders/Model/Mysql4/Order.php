<?php

class MadeAlien_Orders_Model_Mysql4_Order extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("ma_orders/order", "ma_order_log_id");
    }
}