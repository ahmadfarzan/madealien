<?php

class MadeAlien_Orders_Model_Observer
{

    public function log(Varien_Event_Observer $observer)
    {
        if (!Mage::getStoreConfig('madealien_order/general/enabled')) {
            return $this;
        }
        $order = $observer->getPayment()->getOrder();
        $invoice = $observer->getInvoice();
        try {
            $decimalFactor = (float)Mage::getStoreConfig('madealien_order/general/decimal_factor');
            $orderTotal = $order->getGrandTotal();
            $orderTotalPaid = $order->getTotalPaid();
            if ($orderTotal != $orderTotalPaid + $invoice->getGrandTotal()) {
                return $this;
            }
            $data = array(
                'order_id' => $order->getId(),
                'order_increment_id' => $order->getIncrementId(),
                'order_value' => ($decimalFactor * $orderTotal),
            );
            Mage::getModel('ma_orders/order')->setData($data)->setId(null)->save();
        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $this;
    }

}
