<?php

class MadeAlien_Orders_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return true;
    }

    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("ma_orders/order")->_addBreadcrumb(Mage::helper("adminhtml")->__("Order Manager"), Mage::helper("adminhtml")->__("Order Manager"));
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("MadeAlien Orders"));
        $this->_title($this->__("Manager Order"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'made_alien_order.csv';
        $grid = $this->getLayout()->createBlock('ma_orders/adminhtml_order_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportExcelAction()
    {
        $fileName = 'made_alien_order.xls';
        $grid = $this->getLayout()->createBlock('ma_orders/adminhtml_order_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}
