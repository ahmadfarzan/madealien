<?php
$installer = $this;
$installer->startSetup();
$sql=<<<SQLTEXT
CREATE TABLE `ma_order_logs` ( `ma_order_log_id` INT NOT NULL AUTO_INCREMENT , `order_id` INT NOT NULL , `order_increment_id` varchar(255) NOT NULL, `order_value` DOUBLE NOT NULL , `created` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`ma_order_log_id`)) ENGINE = InnoDB;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
